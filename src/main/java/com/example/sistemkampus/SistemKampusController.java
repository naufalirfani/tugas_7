package com.example.sistemkampus;

import com.example.sistemkampus.dto.KelasDTO;
import com.example.sistemkampus.dto.MahasiswaDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SistemKampusController {

    DatabaseService databaseService = new DatabaseService();

    @GetMapping("/sistemkampus")
    public String getBioskop(Model model){

        databaseService.getDataMahasiswa();
        databaseService.getDataMataKuliah();
        databaseService.getDataKelas();

        model.addAttribute("mahasiswaDTOList", databaseService.mahasiswaDTOList);
        model.addAttribute("mataKuliahDTOList", databaseService.mataKuliahDTOList);
        model.addAttribute("kelasDTOList", databaseService.kelasDTOList);
        model.addAttribute("pesan", "");

        return "sistemkampus";
    }

    @PostMapping("/sistemkampus")
    public String postBioskop(Model model,
                              @RequestParam(value = "matakuliah", defaultValue = "") String kodeMk,
                              @RequestParam(value = "mahasiswa", defaultValue = "") String nim,
                              @RequestParam(value = "id", defaultValue = "0") String kelasid,
                              @RequestParam("operasi") String operasi){

        databaseService.getDataMahasiswa();
        databaseService.getDataMataKuliah();
        databaseService.getDataKelas();
        databaseService.getDataMataKuliahByKode(kodeMk);
        databaseService.getDataMahasiswaByNim(nim);

        if(operasi.equals("add")){
            boolean isAmbil = false;
            for(int i = 0; i < databaseService.kelasDTOList.size(); i++){
                KelasDTO kelasDTO = databaseService.kelasDTOList.get(i);
                if(kelasDTO.getMahasiswaDTO().getNim().equals(nim) && kelasDTO.getMataKuliahDTO().getKodeMk().equals(kodeMk)){
                    isAmbil = true;
                    break;
                }
            }
            if(isAmbil){
                String pesan = "Mahasiswa %s sudah mengambil Mata Kuliah %s";
                pesan = String.format(pesan, databaseService.mahasiswaDTO.getNama(), databaseService.mataKuliahDTO.getNama());
                model.addAttribute("pesan", pesan);
            }
            else{
                int sks = databaseService.getDatasks(nim);
                int sksTotal = sks + databaseService.mataKuliahDTO.getSks();
                if(sksTotal >= 24){
                    String pesan = "Mahasiswa %s tidak bisa lagi mengambil Mata Kuliah";
                    pesan = String.format(pesan, databaseService.mahasiswaDTO.getNama());
                    model.addAttribute("pesan", pesan);
                }
                else{
                    databaseService.insertData(kodeMk, nim);
                    model.addAttribute("pesan", "");
                }
            }
        }
        else if(operasi.equals("delete") && Integer.parseInt(kelasid) != 0)
            databaseService.deleteData(Integer.parseInt(kelasid));

        databaseService.getDataKelas();
        model.addAttribute("mahasiswaDTOList", databaseService.mahasiswaDTOList);
        model.addAttribute("mataKuliahDTOList", databaseService.mataKuliahDTOList);
        model.addAttribute("kelasDTOList", databaseService.kelasDTOList);

        return "sistemkampus";
    }
}

package com.example.sistemkampus;

import com.example.sistemkampus.dto.DosenDTO;
import com.example.sistemkampus.dto.KelasDTO;
import com.example.sistemkampus.dto.MahasiswaDTO;
import com.example.sistemkampus.dto.MataKuliahDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseService {

    public List<MahasiswaDTO> mahasiswaDTOList = new ArrayList<>();
    public List<DosenDTO> dosenDTOList = new ArrayList<>();
    public List<MataKuliahDTO> mataKuliahDTOList = new ArrayList<>();
    public List<KelasDTO> kelasDTOList = new ArrayList<>();
    public MahasiswaDTO mahasiswaDTO;
    public MataKuliahDTO mataKuliahDTO;

    public void getDataMahasiswa(){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select * from mahasiswa";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            mahasiswaDTOList.clear();
            while(rs.next()){
                MahasiswaDTO mahasiswaDTO = new MahasiswaDTO();
                mahasiswaDTO.setNim(rs.getString(1));
                mahasiswaDTO.setNama(rs.getString(2));
                mahasiswaDTO.setGender(rs.getString(3));
                mahasiswaDTO.setTempatLahir(rs.getString(4));
                mahasiswaDTO.setTanggalLahir(rs.getDate(5));
                mahasiswaDTOList.add(mahasiswaDTO);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void getDataMahasiswaByNim(String nim){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select * from mahasiswa where nim = '%s'";
            query = String.format(query, nim);

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while(rs.next()){
                mahasiswaDTO = new MahasiswaDTO();
                mahasiswaDTO.setNim(rs.getString(1));
                mahasiswaDTO.setNama(rs.getString(2));
                mahasiswaDTO.setGender(rs.getString(3));
                mahasiswaDTO.setTempatLahir(rs.getString(4));
                mahasiswaDTO.setTanggalLahir(rs.getDate(5));
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void getDataDosen(){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select * from dosen";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            dosenDTOList.clear();
            while(rs.next()){
                String film = String.valueOf(rs.getInt(2));
                String penonton = String.valueOf(rs.getInt(3));

                DosenDTO dosenDTO = new DosenDTO();
                dosenDTO.setNip(rs.getString(1));
                dosenDTO.setNama(rs.getString(2));
                dosenDTO.setGelar(rs.getString(3));
                dosenDTOList.add(dosenDTO);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void getDataMataKuliah(){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select m.*, d.* from dosen d, mata_kuliah m, pengajar p where p.kode_mk = m.kode_mk and p.nip = d.nip;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            mataKuliahDTOList.clear();
            while(rs.next()){
                MataKuliahDTO mataKuliahDTO = new MataKuliahDTO();
                mataKuliahDTO.setKodeMk(rs.getString(1));
                mataKuliahDTO.setNama(rs.getString(2));
                mataKuliahDTO.setSks(rs.getInt(3));
                DosenDTO dosenDTO = new DosenDTO();
                dosenDTO.setNip(rs.getString(4));
                dosenDTO.setNama(rs.getString(5));
                dosenDTO.setGelar(rs.getString(6));
                mataKuliahDTO.setDosenDTO(dosenDTO);
                mataKuliahDTOList.add(mataKuliahDTO);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void getDataMataKuliahByKode(String kode){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select m.*, d.* from dosen d, mata_kuliah m, pengajar p \n" +
                    "where p.kode_mk = m.kode_mk and p.nip = d.nip and m.kode_mk = '%s';";

            query = String.format(query, kode);

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while(rs.next()){
                mataKuliahDTO = new MataKuliahDTO();
                mataKuliahDTO.setKodeMk(rs.getString(1));
                mataKuliahDTO.setNama(rs.getString(2));
                mataKuliahDTO.setSks(rs.getInt(3));
                DosenDTO dosenDTO = new DosenDTO();
                dosenDTO.setNip(rs.getString(4));
                dosenDTO.setNama(rs.getString(5));
                dosenDTO.setGelar(rs.getString(6));
                mataKuliahDTO.setDosenDTO(dosenDTO);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void getDataKelas(){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select m.*, d.*, mh.*, k.id from dosen d, mata_kuliah m, pengajar p, mahasiswa mh, kelas k\n" +
                    "where p.kode_mk = m.kode_mk and p.nip = d.nip and k.nim = mh.nim and k.kode_mk = m.kode_mk;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            kelasDTOList.clear();
            while(rs.next()){
                MataKuliahDTO mataKuliahDTO = new MataKuliahDTO();
                mataKuliahDTO.setKodeMk(rs.getString(1));
                mataKuliahDTO.setNama(rs.getString(2));
                mataKuliahDTO.setSks(rs.getInt(3));
                DosenDTO dosenDTO = new DosenDTO();
                dosenDTO.setNama(rs.getString(4));
                dosenDTO.setNip(rs.getString(5));
                dosenDTO.setGelar(rs.getString(6));
                mataKuliahDTO.setDosenDTO(dosenDTO);
                MahasiswaDTO mahasiswaDTO = new MahasiswaDTO();
                mahasiswaDTO.setNim(rs.getString(7));
                mahasiswaDTO.setNama(rs.getString(8));
                mahasiswaDTO.setGender(rs.getString(9));
                mahasiswaDTO.setTempatLahir(rs.getString(10));
                mahasiswaDTO.setTanggalLahir(rs.getDate(11));

                KelasDTO kelasDTO = new KelasDTO();
                kelasDTO.setMahasiswaDTO(mahasiswaDTO);
                kelasDTO.setMataKuliahDTO(mataKuliahDTO);
                kelasDTO.setId(rs.getInt(12));
                kelasDTOList.add(kelasDTO);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public int getDatasks(String nim){
        int sks = 0;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");
            //here sonoo is database name, root is username and password

            // the mysql insert statement
            String query = "select sum(m.sks) from dosen d, mata_kuliah m, pengajar p, mahasiswa mh, kelas k\n" +
                    "where p.kode_mk = m.kode_mk and p.nip = d.nip and k.nim = mh.nim and k.kode_mk = m.kode_mk and mh.nim = '%s';";

            query = String.format(query, nim);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            kelasDTOList.clear();
            while(rs.next()){
                sks = rs.getInt(1);
            }

            conn.close();
        }catch(Exception e){ System.out.println(e);}

        return sks;
    }

    public void insertData(String kodeMk, String nim){
        try{
            // create a mysql database connection
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");

            // the mysql insert statement
            String query = " insert into kelas (kode_mk, nim)"
                    + " values (?, ?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, kodeMk);
            preparedStmt.setString(2, nim);

            // execute the preparedstatement
            preparedStmt.execute();

            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public void deleteData(int id){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/akademik","root","freetopl4y");

            // the mysql insert statement
            String query = "delete from kelas where id = %d";
            query = String.format(query, id);

            String query2 = "ALTER TABLE `pesanan` AUTO_INCREMENT = 1";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.execute();
            PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
            preparedStmt2.execute();
            conn.close();
        }catch(Exception e){ System.out.println(e);}
    }

    public void updateData(String nama, int atasanId, int companyId, int id){
        try{
            // create a mysql database connection
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/sdm","root","freetopl4y");

            // the mysql insert statement
            String query = "update employee set nama = ?, atasan_id = ?, company_id = ? where id = ?";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, nama);
            preparedStmt.setInt(2, atasanId);
            preparedStmt.setInt(3, companyId);
            preparedStmt.setInt(4, id);

            // execute the preparedstatement
            preparedStmt.execute();

            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}

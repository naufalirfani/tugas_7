package com.example.sistemkampus.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KelasDTO {
    private MataKuliahDTO mataKuliahDTO;
    private MahasiswaDTO mahasiswaDTO;
    private int id;
}

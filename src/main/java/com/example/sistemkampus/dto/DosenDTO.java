package com.example.sistemkampus.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DosenDTO {
    private String nama;
    private String nip;
    private String gelar;
}

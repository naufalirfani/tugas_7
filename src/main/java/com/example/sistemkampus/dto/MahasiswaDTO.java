package com.example.sistemkampus.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MahasiswaDTO {
    private String nim;
    private String nama;
    private String gender;
    private String tempatLahir;
    private Date tanggalLahir;
}

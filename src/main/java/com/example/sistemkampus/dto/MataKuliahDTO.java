package com.example.sistemkampus.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MataKuliahDTO {
    private String kodeMk;
    private String nama;
    private int sks;
    private DosenDTO dosenDTO;
}

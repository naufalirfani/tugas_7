package com.example.sistemkampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemkampusApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemkampusApplication.class, args);
	}

}
